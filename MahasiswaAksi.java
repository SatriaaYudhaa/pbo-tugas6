import mahasiswa.*;

public class MahasiswaAksi {
    public static void main(String[] args) {
        // a. Membuat objek dari class Mahasiswa
        Mahasiswa mahasiswa = new Mahasiswa("A11.2020.13134", Satria Yudha Adhyaksa", 3.60, 84, "2002-02-11");
        // b. Mencari nama program studi berdasarkan nim 
        System.out.println("Nama Program Studi : " + mahasiswa.getProgdi());
        // c. Mencari status berdasarkan ipk
        System.out.println("Status : " + mahasiswa.ipkStatus());
        // d. Mencari tahun angkatan berdasarkan nim 
        System.out.println("Tahun Angkatan : " + mahasiswa.getTahun());
        // e. Mencari tagihan berdasarkan sks
        System.out.println("Tagihan : " + mahasiswa.getTagihanSks());
        // f. Mencari berapa semester mahasiswa sudah kuliah 
        System.out.println("Semester : " + mahasiswa.getMhsSemester());
        // g. Mencari umur mahasiswa
		System.out.println("Umur : " + mahasiswa.getUmur());

    }
}